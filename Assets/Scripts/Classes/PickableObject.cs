﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PickableObject : MonoBehaviour {

    static public int PERK = 0;
    static public int AMMUNITION = 1;

    public enum ObjectType
    {
        HEALTH_RECOVERY,
        AMMO_N,
        AMMO_SHOOT,
        AMMO_RF,
        AMMO_FIRE,
        AMMO_DISP,
        AMMO_GREN,
        AMMO_SHOOT_RF,
        AMMO_SHOOT_DISP,
        AMMO_SHOOT_FLAME,
        AMMO_SHOOT_GREN,
        COIN
    }
    [Header("For Shop")]
    public Sprite icon;
    public int precio;
    public int cantidad;

    [Header("Atributos Objeto")]
    public bool esEfectoSobreTiempo;
    public bool tieneTiempoParaDesaparecer;
    public int duracionEfecto;
    public float duracionVivo;

    public abstract void EfectoInstantaneo(GameObject objetoColisionado);

    public abstract void EfectoSobreTiempo(GameObject objetoColisionado);

    public abstract void EfectoFinal(GameObject objetoColisionado);

    public abstract ObjectType TipoObjeto();

    //Devuelve el valor de -1 si no pertenecee a la tienda
    public abstract int TipoObjetoTienda();

    public virtual void ReaccionAlAparecer()
    {
        //override para cada tip ode comportamiento si es necesario
    }

    private void OnEnable()
    {
        ReaccionAlAparecer();
        if (tieneTiempoParaDesaparecer)
        {
            StartCoroutine(TiempoParaDesaparecer());
        } 
    }

    void OnTriggerEnter(Collider col)
    {
        EfectoInstantaneo(col.gameObject);
        if (esEfectoSobreTiempo && gameObject.activeInHierarchy)
        {
            StartCoroutine(ManejoDuracionEfecto(col.gameObject));
        }
    }

    IEnumerator ManejoDuracionEfecto(GameObject objetoColisionado)
    {
        EfectoSobreTiempo(objetoColisionado);
        yield return new WaitForSeconds(duracionEfecto);
        EfectoFinal(objetoColisionado);
    }

    IEnumerator TiempoParaDesaparecer()
    {
        yield return new WaitForSeconds(duracionVivo);
        gameObject.SetActive(false);
    }
}

[System.Serializable]
public class DropObject
{
    public GameObject objetoDrop;
    public float dropRate;
}
