﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class Zombie : MonoBehaviour
{
    public bool esJefe;
    [HideInInspector]
    public GameState estadoJuego;
    public int salud;
    [HideInInspector]
    public int saludReal;
    public int velocidad;
    public DropObject[] objetosPosibles;

    protected GameObject objetivo;
    protected bool estaMasMuerto;
    protected NavMeshAgent agente;

    public abstract void comportamiento();

    private void Start()
    {
        estadoJuego = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
        agente.speed = velocidad;
    }

    private void Update()
    {
        comportamiento();
    }
}
