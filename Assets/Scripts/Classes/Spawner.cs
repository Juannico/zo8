﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawner : MonoBehaviour
{

    List<GameObject> objetosAcumulados;
    public int tiempoEntreSpawns;
    public int cantidadAcumulada;
    public bool crecera;
    public GameObject objetoAReproducir;
    //public bool esRecogible;

    //protected bool primeraVez = true;
    private float randomN;
    protected float lastUpdate = 0;

    void Start()
    {
        objetosAcumulados = new List<GameObject>();
        for (int i = 0; i < cantidadAcumulada; i++)
        {
            GameObject obj = Instantiate(objetoAReproducir);
            obj.SetActive(false);
            objetosAcumulados.Add(obj);
        }
    }

    public virtual GameObject getPooledObject()
    {
        for (int i = 0; i < objetosAcumulados.Count; i++)
        {
            if (!objetosAcumulados[i].activeInHierarchy)
            {
                cantidadAcumulada--;
                return objetosAcumulados[i];
            }
        }
        
        if (crecera && objetosAcumulados.Count <= cantidadAcumulada)
        {
            GameObject obj = Instantiate(objetoAReproducir);
            objetosAcumulados.Add(obj);
            cantidadAcumulada--;
            return obj;
        }
        return null;
    }

    public abstract void CondicionDeReaparicion();

    public abstract void CondicionDeReaparicion(Transform trans);

    public virtual void CondicionDeReaparicion(Transform trans, int quant)
    {
        //Overrride At convinience
    }

    public virtual void Desaparicion()
    {
        for (int i = 0; i < objetosAcumulados.Count; i++)
        {
            objetosAcumulados[i].SetActive(false);
            objetosAcumulados[i].transform.rotation = Quaternion.identity;
        }
    }

    void Update()
    {
        CondicionDeReaparicion();
    }

}
