﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

    public GameState estadoJuego;
    public int numEnemigosNivel;
    public GameObject jefeFinal;
    public Transform posicionJefe;
    public GameObject[] puertasDeslizantes;
    public List<Transform> puntosAparicion;
    public Transform[] puntosExtra;
    public int numeroNivel;

    //[HideInInspector]
    
    public bool estaPeleaJefe = false;
    //[HideInInspector]
    public bool estaDerrotado = false;
    private GameObject jefeFinalGameObj;

    public void EntrandoANivel()
    {
        estadoJuego.NumEnemigosRestantes += numEnemigosNivel;
        while (numEnemigosNivel>0)
        {
            puntosAparicion[Random.Range(0, puntosAparicion.Count)].GetComponent<Spawner>().cantidadAcumulada++;
            numEnemigosNivel--;
        }
        for (int i = 0; i < puntosAparicion.Count; i++)
        {
            puntosAparicion[i].gameObject.SetActive(true);
        }
        if (puntosExtra.Length != 0)
        {
            for (int i = 0; i < puntosExtra.Length; i++)
            {
                puntosExtra[i].gameObject.SetActive(true);
                estadoJuego.NumEnemigosRestantes += puntosExtra[i].GetComponent<Spawner>().cantidadAcumulada;
            }
        }
    }

    public void ActivarPeleaJefe()
    {
        if (jefeFinal!=null)
        {
            estadoJuego.NumEnemigosRestantes++;
            jefeFinalGameObj = Instantiate(jefeFinal);
            jefeFinalGameObj.GetComponent<Zombie>().esJefe = true;
            jefeFinalGameObj.transform.position = posicionJefe.position;
        }
        else
        {
            estaDerrotado = true;
        }
        if (puertasDeslizantes != null)
        {
            for (int i = 0; i < puertasDeslizantes.Length; i++)
            {
                StartCoroutine(MoverPuerta(puertasDeslizantes[i], jefeFinalGameObj));
            }
        }
        estaPeleaJefe = false;
    }

    IEnumerator MoverPuerta(GameObject puerta,GameObject jefe)
    {
        puerta.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(2f);
        puerta.SetActive(false);
        if (jefe!=null)
        {
            jefe.GetComponent<UnityEngine.AI.NavMeshAgent>().enabled = true;
        }   
    }

    public void LimpiarNivel()
    {
        puntosAparicion = null;
        puertasDeslizantes = null;
        Destroy(jefeFinalGameObj);
    }
}
