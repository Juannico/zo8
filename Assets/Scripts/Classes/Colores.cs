﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Colores
{
    public Material material;
    public int precio;
    public string nomCompuesto;
    public Sprite icon;
    public int numeroColor;
}
