﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlayerData {

    private static PlayerData instance = null;
    private static readonly object padlock = new object();

    protected string saveFile = "";

    public static PlayerData InstanciaData
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new PlayerData();
                }
                return instance;
            }
        }
    }

    public bool tutorialHecho;
    public int vida;
    public int monedas;
    public int actualCheckpoint;
    public Dictionary<PickableObject.ObjectType, int> objetosComprados = new Dictionary<PickableObject.ObjectType, int>();

    public List<string> personajes = new List<string>();
    public List<string> skins = new List<string>();
    public int personajeActual;
    public int perkActual;

    static float r_Version = 1.2f;

    public void AddObjeto(PickableObject.ObjectType type, int cant)
    {
        if (!objetosComprados.ContainsKey(type))
        {
            objetosComprados[type] = 0;
        }

        objetosComprados[type] += cant;

        Save();
    }

    public void AddPersonaje(string nomPersonaje)
    {
        personajes.Add(nomPersonaje);
    }

    public void AddColor(string nomCompuesto)
    {
        skins.Add(nomCompuesto);
    }

    public bool hasSkin(string nameComp)
    {
        for (int i = 0; i < skins.Count; i++)
        {
            if (skins[i].Equals(nameComp))
            {
                return true;
            }
        }
        return false;
    }

    public void CambiarCantidadObjeto(PickableObject.ObjectType objeto, int cuant)
    {
        if (!objetosComprados.ContainsKey(objeto))
        {
            return;
        }
        objetosComprados[objeto] = cuant;
        if (objetosComprados[objeto]<=0)
        {
            objetosComprados.Remove(objeto);
        }
        Save();
    }

    static public void Create()
    {
        InstanciaData.saveFile = Application.persistentDataPath + "/save.bin";

        if (File.Exists(InstanciaData.saveFile))
        {
            InstanciaData.Read();
        }
        else
        {
            NewSave();
        }
    }

    static public void NewSave()
    {
        InstanciaData.tutorialHecho = false;
        InstanciaData.vida = 30;
        InstanciaData.monedas = 1000;
        InstanciaData.actualCheckpoint = 0;
        InstanciaData.personajeActual = 0;

        InstanciaData.personajes.Clear();
        InstanciaData.skins.Clear();
        InstanciaData.objetosComprados.Clear();

        InstanciaData.personajes.Add("Soldier");
        InstanciaData.skins.Add("Soldier|Blue");

        InstanciaData.Save();
    }

    public void Read()
    {
        BinaryReader r = new BinaryReader(new FileStream(saveFile, FileMode.Open));

        r_Version = (float) r.ReadDouble();

        tutorialHecho = r.ReadBoolean();

        vida = r.ReadInt32();
        monedas = r.ReadInt32();

        actualCheckpoint = r.ReadInt32();

        objetosComprados.Clear();
        int contObjetos = r.ReadInt32();
        for (int i = 0; i < contObjetos; i++)
        {
            objetosComprados.Add((PickableObject.ObjectType)r.ReadInt32(),r.ReadInt32());
        }

        personajes.Clear();
        int contPersonajes = r.ReadInt32();
        for (int i = 0; i < contPersonajes; i++)
        {
            string fullNom = r.ReadString();
            personajes.Add(fullNom);
        }

        skins.Clear();
        int contSkins = r.ReadInt32();
        for (int i = 0; i < contSkins; i++)
        {
            string fullNom = r.ReadString();
            skins.Add(fullNom);
        }

        personajeActual = r.ReadInt32();

        //Version 1.2 control
        if (r_Version <= 1.2f)
        {
            tutorialHecho = false;
        }

        r.Close();
    }

    public void Save()
    {
        BinaryWriter w = new BinaryWriter(new FileStream(saveFile,FileMode.OpenOrCreate));

        w.Write((double)r_Version);
        w.Write(tutorialHecho);
        w.Write(vida);
        w.Write(monedas);
        w.Write(actualCheckpoint);

        w.Write(objetosComprados.Count);
        foreach (KeyValuePair<PickableObject.ObjectType,int> obj in objetosComprados)
        {
            w.Write((int)obj.Key);
            w.Write(obj.Value);
        }

        w.Write(personajes.Count);
        foreach (string c in personajes)
        {
            w.Write(c);
        }

        w.Write(skins.Count);
        foreach (string col in skins)
        {
            w.Write(col);
        }

        w.Write(personajeActual);

        w.Close();
    }

}

#if UNITY_EDITOR
public class PlayerDataEditor : Editor
{
    [MenuItem("Zombie 8/Clear Data")]
    static public void ClearData()
    {
        File.Delete(Application.persistentDataPath + "/save.bin");
    }

    [MenuItem("Zombie 8/Dar 10000 Monedas al Jugador")]
    static public void DarMonedas()
    {
        PlayerData.InstanciaData.monedas += 10000;
        PlayerData.InstanciaData.Save();
    }
}
#endif 