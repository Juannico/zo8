﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Personajes", menuName = "Zombie8/Base de Datos Personajes")]
public class CharacterDatabase : ScriptableObject {

    public Character[] personajes;

    static protected Dictionary<string, Character> _diccPersonajes;

    static public Dictionary<string, Character> diccionario { get { return _diccPersonajes; } }

    static public Character ObtenerPersonaje(string alias)
    {
        Character c;
        if (_diccPersonajes == null || !_diccPersonajes.TryGetValue(alias, out c))
        {
            return null;
        }
        return c;
    }

    static public void GuardarPersonaje(string alias, Character chara)
    {
        Character c;
        if (_diccPersonajes == null || !_diccPersonajes.TryGetValue(alias, out c))
        {
            _diccPersonajes.Remove(alias);
            _diccPersonajes.Add(alias, chara);
        }
    }

    public void Cargar()
    {   
        if (_diccPersonajes == null)
        { 
            _diccPersonajes = new Dictionary<string, Character>();
            for (int i =  0; i < personajes.Length; i++)
            {
                _diccPersonajes.Add(personajes[i].aliasPersonaje, personajes[i]);
            }
        }
    }

}
