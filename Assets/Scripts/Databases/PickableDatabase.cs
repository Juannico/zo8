﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Objetos", menuName = "Zombie8/Base de Datos Objetos")]
public class PickableDatabase : ScriptableObject {

    public PickableObject[] perksTienda;

    public PickableObject[] ammoTienda;

    static protected Dictionary<PickableObject.ObjectType, PickableObject> _diccPerksTienda;

    static protected Dictionary<PickableObject.ObjectType, PickableObject> _diccAmmoTienda;

    static public Dictionary<PickableObject.ObjectType, PickableObject> diccionarioPerks { get { return _diccPerksTienda; } }

    static public Dictionary<PickableObject.ObjectType, PickableObject> diccionarioAmmo { get { return _diccAmmoTienda; } }

    static public PickableObject ObtenerPerks(PickableObject.ObjectType tipoObjeto)
    {
        PickableObject c;
        if (_diccPerksTienda == null || !_diccPerksTienda.TryGetValue(tipoObjeto, out c))
        {
            return null;
        }
        return c;
    }

    static public PickableObject ObtenerAmmos(PickableObject.ObjectType tipoObjeto)
    {
        PickableObject c;
        if (_diccAmmoTienda == null || !_diccAmmoTienda.TryGetValue(tipoObjeto, out c))
        {
            return null;
        }
        return c;
    }

    public void Cargar()
    {
        if (_diccPerksTienda == null)
        {
            _diccPerksTienda = new Dictionary<PickableObject.ObjectType, PickableObject>();
            for (int i = 0; i < perksTienda.Length; i++)
            {
                if (perksTienda[i].TipoObjetoTienda() == PickableObject.PERK)
                {
                    _diccPerksTienda.Add(perksTienda[i].TipoObjeto(), perksTienda[i]);
                } 
            }
        }

        if (_diccAmmoTienda == null)
        {
            _diccAmmoTienda = new Dictionary<PickableObject.ObjectType, PickableObject>();
            for (int i = 0; i < ammoTienda.Length; i++)
            {
                if (ammoTienda[i].TipoObjetoTienda() == PickableObject.AMMUNITION)
                {
                    _diccAmmoTienda.Add(ammoTienda[i].TipoObjeto(), ammoTienda[i]);
                }
            }
        }
    }
}
