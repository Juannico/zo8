﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class GameState : States
{
    public GameManager manager;
    public MenuState menu;

    [Header("Canvas")]
    public Canvas canvasHUD;
    public Canvas canvasGameOver;
    public Canvas canvasPause;
    public Canvas canvasToBeCont;

    [Header("UI")]
    public GameObject barraVida;
    public Text numBalasNom;
    public Text numBalasRF;
    public Text numBalasFire;
    public Text numBalasDisp;
    public Text numGrenades;
    public Text textoTiempo;
    public Image[] imagenCambioBalas;
    public Text zombiesRestantes;
    public Image[] imagenesZonas;
    public Image zone;
    public GameObject shootButton;

    [Header("Musica")]
    public AudioSource musicaJuegoLoop;

    [Header("Niveles")]
    public Level[] niveles;

    [Header("Cámaras")]
    public Cinemachine.CinemachineVirtualCamera camaraJugador;
    public Cinemachine.CinemachineVirtualCamera camaraOtros;

    [Header("Tutorial")]
    public Image tutorialImage;

    //float tiempoActual = 0;

    private CharacController jugador;
    [HideInInspector]
    public GameObject flechaSiguientePosicion;
    private int numEnemigosRestantes;
    [HideInInspector]
    public Level nivelActual;
    private bool puntosActualizados;

    //Getter & Setter
    public int NumEnemigosRestantes
    {
        get { return numEnemigosRestantes; }
        set { numEnemigosRestantes = value; }
    }
    // Use this for initialization
    void Start()
    {
        tutorialImage.gameObject.SetActive(false);
        //Advertisement.Initialize("1729720", false);
        Time.timeScale = 1f;
        codigoEstado = 1;
        puntosActualizados = false;
        zone.enabled = false;
    }

    IEnumerator Tutorial()
    {
        //moverse
        yield return new WaitForSeconds(2);
        tutorialImage.gameObject.SetActive(true);
        tutorialImage.CrossFadeAlpha(0.5f, 1f, true);
        Time.timeScale = 0;
        yield return new WaitForSeconds(0.5f);
    }

    public override void Durante()
    {
        //actualizar canvas
        //float t = Time.time - tiempoActual;

        //string minutos = ((int)t / 60).ToString();
        //string segundos = (t % 60).ToString("00");

        //textoTiempo.text = "Time: " + minutos + ":" + segundos;
        barraVida.GetComponent<Image>().fillAmount = (jugador.vida / 100);
        zombiesRestantes.text = numEnemigosRestantes.ToString();
        //Game Over
        if (jugador.vida < 1)
        {
            canvasGameOver.gameObject.SetActive(true);
            Time.timeScale = 0;
            ActualizarPlayerData();
            canvasHUD.gameObject.SetActive(false);
            jugador.enabled = false;
        }
        numBalasNom.text = "x " + jugador.balasNormalDisp.ToString();
        numBalasRF.text = "x " + jugador.balasRapidFire.ToString();
        numBalasDisp.text = "x " + jugador.balasDisp.ToString();
        numBalasFire.text = "x " + jugador.balasFire.ToString();
        numGrenades.text = "x " + jugador.balasGrenades.ToString();

        if (numEnemigosRestantes == 0 && !nivelActual.estaDerrotado)
        {
            nivelActual.estaPeleaJefe = true;
        }
        else if (numEnemigosRestantes == 0 && nivelActual.estaDerrotado && puntosActualizados == false && (nivelActual.numeroNivel + 1) < 9)
        {
            if (nivelActual.numeroNivel > 0)
            {
                for (int i = 0; i < nivelActual.puntosAparicion.Count; i++)
                {
                    niveles[nivelActual.numeroNivel + 1].puntosAparicion.Add(nivelActual.puntosAparicion[i]);
                    imagenesZonas[nivelActual.numeroNivel-1].gameObject.SetActive(false);
                }
                puntosActualizados = true;
                StartCoroutine(ZoneClearUI());
                flechaSiguientePosicion.SetActive(true);
            }
            else
            {
                puntosActualizados = true;
                StartCoroutine(ZoneClearUI());
                CambioNivel();
            }
        }
        if (nivelActual.estaPeleaJefe == true)
        {
            nivelActual.ActivarPeleaJefe();
        }
        if (flechaSiguientePosicion.activeInHierarchy)
        {
            flechaSiguientePosicion.transform.LookAt(niveles[nivelActual.numeroNivel + 1].gameObject.transform);
        }
    }

    IEnumerator ZoneClearUI()
    {
        zone.enabled = true;
        yield return new WaitForSeconds(2);
        zone.enabled = false;
    }

    public override void Entrada()
    {
        jugador = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacController>();
        jugador.currentBullet = 0;
        int temp;
        PlayerData.InstanciaData.objetosComprados.TryGetValue(PickableObject.ObjectType.AMMO_N, out temp);
        jugador.balasNormalDisp = temp;
        if (jugador.balasNormalDisp < 45)
        {
            jugador.balasNormalDisp += 45;
        }
        PlayerData.InstanciaData.objetosComprados.TryGetValue(PickableObject.ObjectType.AMMO_RF, out temp);
        jugador.balasRapidFire = temp;
        PlayerData.InstanciaData.objetosComprados.TryGetValue(PickableObject.ObjectType.AMMO_DISP, out temp);
        jugador.balasDisp = temp;
        PlayerData.InstanciaData.objetosComprados.TryGetValue(PickableObject.ObjectType.AMMO_FIRE, out temp);
        jugador.balasFire = temp;
        PlayerData.InstanciaData.objetosComprados.TryGetValue(PickableObject.ObjectType.AMMO_GREN, out temp);
        jugador.balasGrenades = temp;
        flechaSiguientePosicion = jugador.gameObject.transform.GetChild(4).gameObject;
        if (PlayerData.InstanciaData.tutorialHecho == true)
        {
            nivelActual = niveles[1];
        }
        else
        {
            nivelActual = niveles[0];
            StartCoroutine(Tutorial());
        }
        if (nivelActual.numeroNivel > 0)
        {
            imagenesZonas[nivelActual.numeroNivel-1].color = Color.red;
        }
        nivelActual.EntrandoANivel();
        //tiempoActual = Time.time;
        canvasHUD.gameObject.SetActive(true);
        musicaJuegoLoop.gameObject.SetActive(true);
        musicaJuegoLoop.Play();
        jugador.jsMovement = GameObject.FindGameObjectWithTag("Joystick").GetComponent<VJHandler>();
        camaraJugador.m_LookAt = jugador.transform;
        camaraJugador.m_Follow = jugador.transform;
        imagenCambioBalas[0].color = new Color(255, 255, 255, 255);
    }

    public override void Salida()
    {
        canvasHUD.gameObject.SetActive(false);
        canvasGameOver.gameObject.SetActive(false);
        musicaJuegoLoop.Stop();
        musicaJuegoLoop.gameObject.SetActive(false);
    }

    public void CambioNivel()
    {
        nivelActual.LimpiarNivel();
        if (nivelActual.numeroNivel + 1 < 9)
        {
            nivelActual = niveles[nivelActual.numeroNivel + 1];
            nivelActual.EntrandoANivel();
            imagenesZonas[nivelActual.numeroNivel-1].color = Color.red;
            puntosActualizados = false;
        }
        else
        {
            activarToBeContinued();
        }
    }

    public void RecargaEscena()
    {
        ActualizarPlayerData();
        //Advertisement.Show();
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }

    public void activarPausa()
    {
        canvasHUD.gameObject.SetActive(false);
        canvasPause.gameObject.SetActive(true);
        Time.timeScale = 0;
        System.GC.Collect();
    }

    public void desactivarPausa()
    {
        canvasHUD.gameObject.SetActive(true);
        canvasPause.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void activarToBeContinued()
    {
        ActualizarPlayerData();
        canvasHUD.gameObject.SetActive(false);
        canvasPause.gameObject.SetActive(false);
        canvasToBeCont.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void MakeCharShoot()
    {
        jugador.Shoot();
    }

    public void StopCharShooting()
    {
        jugador.StopShooting();
    }

    public void ActualizarPlayerData()
    {
        if (jugador != null)
        {
            PlayerData.InstanciaData.CambiarCantidadObjeto(PickableObject.ObjectType.AMMO_N, jugador.balasNormalDisp);
            PlayerData.InstanciaData.CambiarCantidadObjeto(PickableObject.ObjectType.AMMO_RF, jugador.balasRapidFire);
            PlayerData.InstanciaData.CambiarCantidadObjeto(PickableObject.ObjectType.AMMO_FIRE, jugador.balasFire);
            PlayerData.InstanciaData.CambiarCantidadObjeto(PickableObject.ObjectType.AMMO_GREN, jugador.balasGrenades);
            PlayerData.InstanciaData.CambiarCantidadObjeto(PickableObject.ObjectType.AMMO_DISP, jugador.balasDisp);
            PlayerData.InstanciaData.Save();
        }
    }

    //cod = 0; el jugador cambia a tipo de bala normal
    //cod = 1; El jugador cambio a tipo de bala automatica
    public void CambioEstadoUI(int cod)
    {
        if (cod >= 0 && cod < 5)
        {
            if (jugador.currentBullet != cod)
            {
                imagenCambioBalas[cod].color = new Color(255, 255, 255, 1); ;
                imagenCambioBalas[jugador.currentBullet].color = new Color(255, 255, 255, 0.5f);
                if (cod == 1 || cod == 3)
                {
                    jugador.currentBullet = cod;
                    shootButton.GetComponent<UIButtonHandler>().enabled = true;
                    shootButton.GetComponent<Button>().enabled = false;
                }
                else
                {
                    shootButton.GetComponent<UIButtonHandler>().enabled = false;
                    shootButton.GetComponent<Button>().enabled = true;
                }
                jugador.currentBullet = cod;
            }  
        }
    }

    public void TurnOfftutorial()
    {
        tutorialImage.CrossFadeAlpha(1, 1, true);
        tutorialImage.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
}
