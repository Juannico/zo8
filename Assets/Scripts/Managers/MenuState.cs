﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using Cinemachine;

public class MenuState : States
{
    public GameManager manager;

    [Header("Canvas")]
    public Canvas canvasMainMenu;
    public Canvas canvasCreditos;
    [Header("UI")]
    public Image canvasFade;
    public Text coinText;

    [Header("Camaras")]
    public CinemachineVirtualCamera menuCamera;
    public CinemachineVirtualCamera loadoutCamera;
    public CinemachineVirtualCamera playCamera;
    public CinemachineVirtualCamera creditsCamera;

    [Header("Music")]
    public AudioSource musicaIntro;

    public GameObject personajeCargado;
    public Transform padrePersonaje;

    protected bool estaCargandoPersonaje;
    void Start()
    {
        codigoEstado = 0;
    }

    void FadeIn()
    {
        canvasFade.CrossFadeAlpha(0, 1f, true);
    }

    public override void Entrada()
    {
        StartCoroutine(waitForLoad());
        StartCoroutine(CargarPersonajes());
        musicaIntro.gameObject.SetActive(true);
        canvasMainMenu.gameObject.SetActive(true);
        musicaIntro.Play();
        menuCamera.gameObject.SetActive(true);
        playCamera.gameObject.SetActive(false);
        loadoutCamera.gameObject.SetActive(false);
    }

    public override void Salida()
    {
        canvasMainMenu.gameObject.SetActive(false);
        canvasCreditos.gameObject.SetActive(false);
        musicaIntro.Stop();
        musicaIntro.gameObject.SetActive(false);
    }

    public override void Durante()
    {
        coinText.text = PlayerData.InstanciaData.monedas.ToString();
    }
    public void IrATienda()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(2, UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }

    public void CambioEstadoInterfaz(int numOpcion)
    {
        //Cambia al menu principal
        if (numOpcion == 0)
        {
            creditsCamera.gameObject.SetActive(false);
            menuCamera.gameObject.SetActive(true);
            loadoutCamera.gameObject.SetActive(false);
            playCamera.gameObject.SetActive(false);
            Character temp = personajeCargado.GetComponent<Character>();
            if (!PlayerData.InstanciaData.hasSkin(temp.colores[temp.colorActual].nomCompuesto))
            {
                for (int i = 0; i < temp.colores.Length; i++)
                {
                    if (PlayerData.InstanciaData.hasSkin(temp.colores[i].nomCompuesto))
                    {
                        CambioColor(temp.colores[i].numeroColor);
                    }
                }
            }
        }
        //Cambia a la pantalla de equipamiento
        else if (numOpcion == 1)
        {
            creditsCamera.gameObject.SetActive(false);
            menuCamera.gameObject.SetActive(false);
            loadoutCamera.gameObject.SetActive(true);
            playCamera.gameObject.SetActive(false);
            StartCoroutine(waitForLoad1());
        }
        //Cambia a la pantalla de los creditos
        else if (numOpcion == 2)
        {
            creditsCamera.gameObject.SetActive(true);
            menuCamera.gameObject.SetActive(false);
            loadoutCamera.gameObject.SetActive(false);
            playCamera.gameObject.SetActive(false);
        }
        //Cambia a la camara del juego
        else if (numOpcion == 3)
        {
            creditsCamera.gameObject.SetActive(false);
            menuCamera.gameObject.SetActive(false);
            loadoutCamera.gameObject.SetActive(false);
            playCamera.gameObject.SetActive(true);
            manager.cambiarEstado(1);
        }
    }

    public void CambioColor(int numColor)
    {
        if (personajeCargado == null)
        {
            return;
        }
        Character c = personajeCargado.GetComponent<Character>();
        if (c.colores.Length != 0)
        {

            if (c.colores[numColor] != null)
            {
                c.colorActual = numColor;
                c.rendererColor.material = c.colores[c.colorActual].material;
            }
        }
    }

    public void ChangeCharacter(int dir)
    {
        PlayerData.InstanciaData.personajeActual += dir;
        if (PlayerData.InstanciaData.personajeActual >= PlayerData.InstanciaData.personajes.Count)
            PlayerData.InstanciaData.personajeActual = 0;
        else if (PlayerData.InstanciaData.personajeActual < 0)
            PlayerData.InstanciaData.personajeActual = PlayerData.InstanciaData.personajes.Count - 1;

        PlayerData.InstanciaData.Save();
        StartCoroutine(CargarPersonajes());
    }



    IEnumerator CargarPersonajes()
    {
        if (personajeCargado != null)
        {
            Destroy(personajeCargado.gameObject);
            personajeCargado = null;
        }
        if (!estaCargandoPersonaje)
        {
            estaCargandoPersonaje = true;
            GameObject nPer = null;
            while (nPer == null)
            {
                Character c = CharacterDatabase.ObtenerPersonaje(PlayerData.InstanciaData.personajes[PlayerData.InstanciaData.personajeActual]);
                if (c != null)
                {
                    if (personajeCargado != null)
                    {
                        Destroy(personajeCargado);
                    }
                    nPer = Instantiate(c.gameObject, padrePersonaje, true);
                    personajeCargado = nPer;
                    personajeCargado.transform.localPosition = Vector3.forward * 1000;
                    personajeCargado.transform.localRotation = c.gameObject.transform.rotation;
                    yield return new WaitForEndOfFrame();
                    personajeCargado.transform.localPosition = Vector3.zero;

                }
                else
                    yield return new WaitForSeconds(1f);
            }
            estaCargandoPersonaje = false;
        }
    }

    IEnumerator waitForLoad()
    {
        yield return new WaitForSeconds(2f);
        FadeIn();
    }

    IEnumerator waitForLoad1()
    {
        yield return new WaitForSeconds(2f);
        IrATienda();
    }

    public void ShowRewardedAd()
    {
        //if (Advertisement.IsReady("rewardedVideo"))
        //{
        //    var options = new ShowOptions { resultCallback = HandleShowResult };
        //    Advertisement.Show("rewardedVideo", options);
        //}
    }

    //public void HandleShowResult(ShowResult result)
    //{
    //    if (result == ShowResult.Finished)
    //    {
    //        PlayerData.InstanciaData.monedas += 100;
    //        PlayerData.InstanciaData.Save();
    //    }
    //    else if (result == ShowResult.Failed)
    //    {

    //    }
    //}
}
