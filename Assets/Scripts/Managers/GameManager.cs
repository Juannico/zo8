﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {


    public States[] estados;
    public CharacterDatabase _basePersonajes;
    public PickableDatabase _baseObjetos;

    private List<States> r_estados;
    private States actual;

    // Se inicializa la lista de estados teniendo en cuenta que la lista publica se elimina para evitar que el usuario intervenga en ella. Tambien ahorra espacio.
    void Start () {
        
        PlayerData.Create();
       _basePersonajes.Cargar();
        _baseObjetos.Cargar();
        r_estados = new List<States>();
        for (int i = 0; i < estados.Length; i++)
        {
            r_estados.Add(estados[i]);
        }
        estados = null;
        actual = r_estados[0];
        actual.Entrada();
    }
	
	// Se encarga de hacer loop en cualquier cosas que necesite hace el estado actual

	void Update () {
        actual.Durante();
	}

    public void cambiarEstado(int codigo)
    {
        for (int i = 0; i < r_estados.Count; i++)
        {
            if (r_estados[i].codigoEstado == codigo)
            {
                actual.Salida();
                r_estados[i].Entrada();
                actual = r_estados[i];
            }
            
        }
    }
}

public abstract class States : MonoBehaviour
{

    public int codigoEstado;

    public abstract void Entrada();

    public abstract void Salida();

    public abstract void Durante();
}
