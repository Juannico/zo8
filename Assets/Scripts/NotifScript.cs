﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Assets.SimpleAndroidNotifications
{
	public class NotifScript : MonoBehaviour 
	{
        public static bool isCreated = false;
		// Use this for initialization
		void Awake () 
		{
            if (!isCreated)
            {
                DontDestroyOnLoad(this.gameObject);
                isCreated = true;
                NotificationManager.SendWithAppIcon(TimeSpan.FromHours(24), "Zombie notification", "Zombies are out of control, please go back and find the cure", Color.white, NotificationIcon.Message);
                NotificationManager.SendWithAppIcon(TimeSpan.FromHours(48), "Zombie notification", "Go defend the city from the zombies", Color.red, NotificationIcon.Message);
                NotificationManager.SendWithAppIcon(TimeSpan.FromDays(8), "Zombie notification", "Time to fight! Go back to defend our city", Color.red, NotificationIcon.Message);
                //isLoop = false;
            }

        }
	
	}
}
