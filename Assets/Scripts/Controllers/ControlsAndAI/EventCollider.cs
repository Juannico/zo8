﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCollider : MonoBehaviour {

    public GameObject sangreJugador;
    public int cantidadDaño;
    private GameState estadoJuego;
    public enum TipoEvento
    {
        DAÑO,
        CAMBIO_NIVEL
    }
    public TipoEvento evento;

    private void Start()
    {
        estadoJuego = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
    }
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (evento == TipoEvento.DAÑO)
        {
            if (other.CompareTag("Player"))
            {
                other.gameObject.GetComponent<CharacController>().vida -= cantidadDaño;
                GameObject aux = Instantiate(sangreJugador, other.transform.position + Vector3.up, Quaternion.identity);
                Destroy(aux, 3f);
            }
        }
        else if (evento == TipoEvento.CAMBIO_NIVEL)
        {
            if (other.CompareTag("Player"))
            {
                estadoJuego.flechaSiguientePosicion.SetActive(false);
                estadoJuego.CambioNivel();
                gameObject.SetActive(false);
            }
        }
    }
}
