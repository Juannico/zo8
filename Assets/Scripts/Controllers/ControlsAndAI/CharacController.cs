﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacController : MonoBehaviour {
    public float velocidadGiro;
    public float velocidad;
    public float vida;
    public Transform salidaDeBala;
    public float tiempo;
    public VJHandler jsMovement;
    public AudioClip sonidoBalaNormal;
    public AudioClip sonidoBalaRafaga;
    public AudioClip sonidoBalaDisp;
    public AudioClip sonidoBalaFlama;
    public AudioClip sonidoBalaGrenade;

    [HideInInspector]
    public int balasNormalDisp;
    [HideInInspector]
    public int balasRapidFire;
    [HideInInspector]
    public int balasFire;
    [HideInInspector]
    public int balasDisp;
    [HideInInspector]
    public int balasGrenades;
    protected Animator anim;

    private Vector3 direction;
    private Spawner r_shootSpawner;
    private Spawner r_shootRFSpawner;
    private Spawner r_shootDispSpawner;
    private Spawner r_shootFlameSpawner;
    private Spawner r_shootGrenadeSpawner;
    private AudioSource c_Audio;
    [HideInInspector]
    public int currentBullet;

    void OnEnable()
    {
        c_Audio = GetComponent<AudioSource>();
        r_shootSpawner = GameObject.FindGameObjectWithTag("GenBalas").transform.GetChild(0).GetComponent<Spawner>();
        r_shootRFSpawner = GameObject.FindGameObjectWithTag("GenBalas").transform.GetChild(1).GetComponent<Spawner>();
        r_shootDispSpawner = GameObject.FindGameObjectWithTag("GenBalas").transform.GetChild(2).GetComponent<Spawner>();
        r_shootFlameSpawner = GameObject.FindGameObjectWithTag("GenBalas").transform.GetChild(3).GetComponent<Spawner>();
        r_shootGrenadeSpawner = GameObject.FindGameObjectWithTag("GenBalas").transform.GetChild(4).GetComponent<Spawner>();
        tiempo = Time.time;
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (jsMovement != null)
        {
		direction = jsMovement.InputDirection; 
		if (Input.GetKey(KeyCode.L))
         {
                Shoot();
         }
		if (direction.magnitude != 0) {
			//MovimientoPersonaje
			if (direction.y > 0) {
                anim.SetFloat("Speed", direction.y);
                anim.SetFloat("SpeedBck", 0);
                transform.Translate (new Vector3 (0, 0, velocidad * direction.y) * Time.deltaTime);
			}
            else if (direction.y < 0)
            {
                anim.SetFloat("Speed", 0);
                anim.SetFloat("SpeedBck", -direction.y);
                transform.Translate(new Vector3(0, 0, velocidad * direction.y * 0.6f) * Time.deltaTime);
            }
			//MovimientoCamara
			if (direction.x <= 0.5f && direction.x >= -0.5f ) {
				transform.Rotate (0, direction.x * Time.deltaTime * velocidadGiro * 0.35f, 0);
			}
            else
            {
                transform.Rotate(0, direction.x * Time.deltaTime * velocidadGiro, 0);
            }

		} else {
			anim.SetFloat("Speed", 0);
            anim.SetFloat("SpeedBck", 0);
        }
        }
    }

    public void Shoot()
    {
        if (tiempo < Time.time - 0.5f)
        {
            if (currentBullet == 0 && balasNormalDisp > 0)
            {
                c_Audio.loop = false;
                c_Audio.clip = sonidoBalaNormal;
                c_Audio.Play();
                balasNormalDisp--;
                r_shootSpawner.CondicionDeReaparicion(salidaDeBala);
                if (direction.magnitude == 0)
                {
                    anim.SetTrigger("Instance");
                }
                tiempo = Time.time;
            }
            else if (currentBullet == 1 && balasRapidFire > 0)
            {
                c_Audio.clip = sonidoBalaRafaga;
                c_Audio.loop = true;
                if (!c_Audio.isPlaying)
                {
                    c_Audio.Play();
                }
                balasRapidFire--;
                r_shootRFSpawner.CondicionDeReaparicion(salidaDeBala);
                if (direction.magnitude == 0)
                {
                    anim.SetTrigger("Instance");
                }
                tiempo = Time.time - 0.35f;
            }
            else if (currentBullet == 2 && balasDisp > 0)
            {
                c_Audio.loop = false;
                c_Audio.clip = sonidoBalaDisp;
                c_Audio.Play();
                balasDisp--;
                r_shootDispSpawner.CondicionDeReaparicion(salidaDeBala,5);
                if (direction.magnitude == 0)
                {
                    anim.SetTrigger("Instance");
                }
                tiempo = Time.time + 0.3f;
            }
            else if (currentBullet == 3 && balasFire > 0)
            {
                c_Audio.clip = sonidoBalaFlama;
                c_Audio.loop = true;
                if (!c_Audio.isPlaying)
                {
                    c_Audio.Play();
                }
                r_shootFlameSpawner.CondicionDeReaparicion(salidaDeBala);
                balasFire--;
                tiempo = Time.time - 0.2f;
            }
            else if (currentBullet == 4 && balasGrenades > 0)
            {
                c_Audio.loop = false;
                c_Audio.clip = sonidoBalaGrenade;
                c_Audio.Play();
                r_shootGrenadeSpawner.CondicionDeReaparicion(salidaDeBala);
                balasGrenades--;
                if (direction.magnitude == 0)
                {
                    anim.SetTrigger("Instance");
                }
                tiempo = Time.time + 1f;
            }     
        }
    }

    public void StopShooting()
    {
        r_shootFlameSpawner.Desaparicion();
        c_Audio.Stop();
    }
}
