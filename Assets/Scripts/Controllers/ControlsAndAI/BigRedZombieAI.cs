﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BigRedZombieAI : Zombie
{
    private GameObject Sold2;
    Animator dead;
    private bool muerto;
    public GameObject explo;
    //ataque
    private float tAtaque;
    private GameObject bala;
    public AudioSource deadZombie;
    public GameObject dañoMano;

    private GameObject potionSpawner;

    private void Awake()
    {
        potionSpawner = GameObject.FindGameObjectWithTag("GenCure");
        agente = GetComponent<NavMeshAgent>();
        if (objetosPosibles != null && objetosPosibles.Length > 0)
        {
            objetosPosibles[0].objetoDrop = potionSpawner;
        }
    }

    void OnEnable()
    {
        deadZombie = GameObject.FindGameObjectWithTag("AudioMuertoZombie").GetComponent<AudioSource>();
        saludReal = salud;
        Sold2 = GameObject.FindGameObjectWithTag("Player");
        dead = gameObject.GetComponent<Animator>();
        tAtaque = Time.time;
        GetComponent<Collider>().enabled = true;
        GetComponent<NavMeshAgent>().isStopped = false;
        GetComponentInChildren<BoxCollider>(false);
        muerto = false;
        dañoMano.GetComponent<BoxCollider>().enabled = true;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "BalaAzul")
        {
            saludReal = (saludReal - 1);
            GameObject sangreTemp = Instantiate(explo, col.transform.position, Quaternion.identity);
            Destroy(sangreTemp, 2f);
        }
    }
    public void botarObjeto()
    {
        for (int i = 0; i < objetosPosibles.Length; i++)
        {
            int randNum = Random.Range(0, 100);
            if (randNum <= objetosPosibles[i].dropRate)
            {
                objetosPosibles[i].objetoDrop.GetComponent<Spawner>().CondicionDeReaparicion(transform);
            }

        }
    }

    IEnumerator waitForAnim()
    {
        muerto = true;
        GetComponent<Collider>().enabled = false;
        GetComponent<NavMeshAgent>().isStopped = true;
        estadoJuego.NumEnemigosRestantes--;
        if (objetosPosibles.Length > 0)
        {
            botarObjeto();
        }
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);

    }

    public override void comportamiento()
    {
        if (!muerto)
        {

            if (Vector3.Distance(transform.position, Sold2.transform.position) < 4f)
            {
                GetComponent<NavMeshAgent>().isStopped = true;
                dead.SetBool("Cerca", true);
                if (tAtaque < Time.time - 2)
                {
                    dead.SetTrigger("Atacar");
                    tAtaque = Time.time;
                }
                transform.LookAt(Sold2.transform);
            }
            else
            {
                if (esJefe)
                {
                    estadoJuego.nivelActual.estaDerrotado = true;
                }
                dead.SetBool("Cerca", false);
                if (GetComponent<NavMeshAgent>().enabled == true)
                {
                    GetComponent<NavMeshAgent>().destination = Sold2.transform.position;
                    GetComponent<NavMeshAgent>().isStopped = false;
                }
            }
            if (saludReal <= 0)
            {
                dañoMano.GetComponent<BoxCollider>().enabled = false;
                dead.SetBool("vida", true);
                deadZombie.Play();
                StartCoroutine(waitForAnim());
            }
        }
    }
}
