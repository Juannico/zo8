﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GreenZombieAI : Zombie
{

    public GameObject explo;

    public AudioSource deadZombie;
    public GameObject dañoMano;

    private GameObject Sold2;
    Animator dead;
    private bool muerto;
    private float tAtaque;
    private NavMeshObstacle navObs;
    private GameObject bala;
    private GameObject ammoSpawner;
    private GameObject heartSpawner;
    private GameObject sHeartSpawner;
    private GameObject coinSpawner;
    private void Awake()
    {
        ammoSpawner = GameObject.FindGameObjectWithTag("GeneradorMunicion");
        heartSpawner = GameObject.FindGameObjectWithTag("GeneradorCorazon");
        sHeartSpawner = GameObject.FindGameObjectWithTag("GenCorazonPequ");
        coinSpawner = GameObject.FindGameObjectWithTag("CoinSpawner");
        objetosPosibles[0].objetoDrop = ammoSpawner;
        objetosPosibles[1].objetoDrop = heartSpawner;
        objetosPosibles[2].objetoDrop = sHeartSpawner;
        objetosPosibles[3].objetoDrop = coinSpawner;
        agente = GetComponent<NavMeshAgent>();
        navObs = GetComponent<NavMeshObstacle>();
    }

    void OnEnable()
    {
        deadZombie = GameObject.FindGameObjectWithTag("AudioMuertoZombie").GetComponent<AudioSource>();
        saludReal = salud;
        Sold2 = GameObject.FindGameObjectWithTag("Player") ;
        dead = gameObject.GetComponent<Animator>();
        tAtaque = Time.time;
        GetComponent<Collider>().enabled = true;
        navObs.enabled = false;
        agente.enabled = true;
        GetComponentInChildren<BoxCollider>(false);
        muerto = false;
        dañoMano.GetComponent<BoxCollider>().enabled = false;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "BalaAzul")
        {
            saludReal = (saludReal - 1);
            GameObject sangreTemp = Instantiate(explo, col.transform.position, Quaternion.identity);
            Destroy(sangreTemp, 2f);
        }
    }
    public void botarObjeto()
    {
        for (int i = 0; i < objetosPosibles.Length; i++)
        {
            float randNum = Random.Range(0,100);
            if (randNum <= objetosPosibles[i].dropRate)
            {
                objetosPosibles[i].objetoDrop.GetComponent<Spawner>().CondicionDeReaparicion(transform);
            }
            
        }
    }

    IEnumerator waitForAnim()
    {
        muerto = true;
        GetComponent<Collider>().enabled = false;
        agente.enabled = false;
        navObs.enabled = true;
        estadoJuego.NumEnemigosRestantes--;
        if (objetosPosibles.Length > 0)
        {
            botarObjeto();
        }
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);   
    }

    IEnumerator waitForAttack()
    {
        dañoMano.GetComponent<BoxCollider>().enabled = true;
        dead.SetTrigger("Atacar");
        yield return new WaitForSeconds(1f);
        dañoMano.GetComponent<BoxCollider>().enabled = false;

    }

    public override void comportamiento()
    {
        if (!muerto)
        {
            
            if (Vector3.Distance(transform.position, Sold2.transform.position) < 2.5f)
            {
                transform.LookAt(Sold2.transform);
                agente.enabled = false;
                navObs.enabled = true;
                dead.SetBool("Cerca", true);
                if (tAtaque < Time.time - 2)
                {
                    StartCoroutine(waitForAttack());
                    tAtaque = Time.time;
                }
            }
            else
            {
                dañoMano.GetComponent<BoxCollider>().enabled = false;
                navObs.enabled = false;
                agente.enabled = true; 
                dead.SetBool("Cerca", false);
                if (agente.enabled == true)
                {
                    agente.destination = Sold2.transform.position;
                    agente.isStopped = false;
                }
            }
            if (saludReal <= 0)
            {
                dañoMano.GetComponent<BoxCollider>().enabled = false;
                dead.SetBool("vida", true);
                deadZombie.Play();
                StartCoroutine(waitForAnim());
            }
        }
    }
}
