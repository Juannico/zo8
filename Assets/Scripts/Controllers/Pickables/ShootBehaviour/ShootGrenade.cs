﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootGrenade : PickableObject {

    private Transform salidaBala;
    private Rigidbody rig;
    private Transform direccion;
    public AudioSource t_Audio;
    public ParticleSystem parts;
    public MeshRenderer[] meshes;

    public override void ReaccionAlAparecer()
    {
        base.ReaccionAlAparecer();
        for (int i = 0; i < meshes.Length; i++)
        {
            meshes[i].enabled = true;
        }
        gameObject.GetComponent<BoxCollider>().enabled = true;
        
        if (salidaBala == null || rig == null)
        {
            salidaBala = GameObject.FindGameObjectWithTag("Player").transform;
            rig = GetComponent<Rigidbody>();
            rig.useGravity = true;
            rig.AddForce(salidaBala.TransformDirection(0,0,30),ForceMode.VelocityChange);
            StartCoroutine(ExplodeAfter(duracionVivo -2f));
        }
        else
        {
            rig.useGravity = true;
            rig.AddForce(salidaBala.TransformDirection(0, 0, 30), ForceMode.VelocityChange);
            StartCoroutine(ExplodeAfter(duracionVivo - 2f));
        }
            
    }

    public override void EfectoFinal(GameObject objetoColisionado)
    {

    }

    public override void EfectoInstantaneo(GameObject objetoColisionado)
    {
        if (objetoColisionado.layer == 11)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
            rig.useGravity = false;
            rig.velocity = Vector3.zero;
            parts.Play();
            Collider[] tempCol = Physics.OverlapSphere(transform.position, 9f, 1 << 11);
            for (int i = 0; i < tempCol.Length; i++)
            {
                tempCol[i].gameObject.GetComponent<Zombie>().saludReal -= 10;
            }
            t_Audio.Play();
            StopAllCoroutines();
            StartCoroutine(Desaparece(2f));
        }
    }

    public override void EfectoSobreTiempo(GameObject objetoColisionado)
    {
        //flyyyy
    }

    public override ObjectType TipoObjeto()
    {
        return ObjectType.AMMO_SHOOT_GREN;
    }

    public override int TipoObjetoTienda()
    {
        return -1;
    }

    IEnumerator ExplodeAfter(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        gameObject.GetComponent<BoxCollider>().enabled = false;
        rig.useGravity = false;
        parts.Play();
        t_Audio.Play();
        Collider[] tempCol = Physics.OverlapSphere(transform.position, 9f, 1 << 11);
        for (int i = 0; i < tempCol.Length; i++)
        {
            tempCol[i].gameObject.GetComponent<Zombie>().saludReal -= 10;
        }
        for (int i = 0; i < meshes.Length; i++)
        {
            meshes[i].enabled = false;
        }
    }
    IEnumerator Desaparece(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        gameObject.SetActive(false);
        for (int i = 0; i < meshes.Length; i++)
        {
            meshes[i].enabled = false;
        }
    }
}
