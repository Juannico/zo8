﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFlames : PickableObject {

    private Transform salidaBala;
    private Transform direccion;
    public ParticleSystem[] particles;
    private float tiempo;

    public override void ReaccionAlAparecer()
    {
        base.ReaccionAlAparecer();
        if (salidaBala == null)
        {
            salidaBala = GameObject.FindGameObjectWithTag("Player").transform;
            transform.position = new Vector3(salidaBala.position.x, salidaBala.position.y + 1.8f, salidaBala.position.z);
            transform.rotation = salidaBala.rotation;
            for (int i = 0; i < particles.Length; i++)
            {
                particles[i].Play();
            }
        }
        else
        {
            salidaBala = GameObject.FindGameObjectWithTag("Player").transform;
            transform.position = new Vector3(salidaBala.position.x, salidaBala.position.y + 1.8f, salidaBala.position.z);
            transform.rotation = salidaBala.rotation;
            for (int i = 0; i < particles.Length; i++)
            {
                particles[i].Play();
            }
        }
    }

    public override void EfectoFinal(GameObject objetoColisionado)
    {

    }

    public override void EfectoInstantaneo(GameObject objetoColisionado)
    {
        
    }

    public override void EfectoSobreTiempo(GameObject objetoColisionado)
    {
        
        
    }
    private void OnTriggerStay(Collider other)
    {
            if (other.gameObject.layer == 11 && tiempo < Time.time - 0.2f)
            {
                other.gameObject.GetComponent<Zombie>().saludReal--;
                tiempo = Time.time;
            }
    }

    public override ObjectType TipoObjeto()
    {
        return ObjectType.AMMO_SHOOT_FLAME;
    }

    public override int TipoObjetoTienda()
    {
        return -1;
    }
    private void Update()
    {
        transform.position = new Vector3(salidaBala.position.x, salidaBala.position.y + 1.8f, salidaBala.position.z);
        transform.rotation = salidaBala.rotation;
    }
}
