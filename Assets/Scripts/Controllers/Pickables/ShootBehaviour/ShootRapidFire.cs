﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootRapidFire : PickableObject {
    private Transform salidaBala;
    private Rigidbody rig;
    private Transform direccion;

    public override void ReaccionAlAparecer()
    {
        base.ReaccionAlAparecer();
        if (salidaBala == null || rig == null)
        {
            salidaBala = GameObject.FindGameObjectWithTag("Player").transform;
            rig = GetComponent<Rigidbody>();
            rig.velocity = salidaBala.TransformDirection(new Vector3(0, 0, 30f));
        }
        else
            rig.velocity = salidaBala.TransformDirection(new Vector3(0, 0, 30f));
    }

    public override void EfectoFinal(GameObject objetoColisionado)
    {
        rig.velocity = Vector3.zero;
        gameObject.SetActive(false);
    }

    public override void EfectoInstantaneo(GameObject objetoColisionado)
    {
        if (objetoColisionado.layer == 11)
        {
            rig.velocity = Vector3.zero;
            gameObject.SetActive(false);
        }
    }

    public override void EfectoSobreTiempo(GameObject objetoColisionado)
    {
        //flyyyy
    }

    public override ObjectType TipoObjeto()
    {
        return ObjectType.AMMO_SHOOT_RF;
    }

    public override int TipoObjetoTienda()
    {
        return -1;
    }

}
