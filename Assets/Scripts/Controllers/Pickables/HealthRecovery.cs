﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRecovery : PickableObject {

    public int cantidadDeVidaARecuperar;

    public override void EfectoFinal(GameObject objetoColisionado)
    {
        gameObject.SetActive(false);
    }

    public override void EfectoInstantaneo(GameObject objetoColisionado)
    {
        if (objetoColisionado.CompareTag("Player"))
        {
            if (objetoColisionado.GetComponent<CharacController>().vida >= 100)
            {
                //cant heal more
            }
            else if ((objetoColisionado.GetComponent<CharacController>().vida + cantidadDeVidaARecuperar) > 100)
            {
                objetoColisionado.GetComponent<CharacController>().vida = 100;
                gameObject.SetActive(false);
            }
            else
            {
                objetoColisionado.GetComponent<CharacController>().vida += cantidadDeVidaARecuperar;
                gameObject.SetActive(false);
            } 
        }
    }

    public override void EfectoSobreTiempo(GameObject objetoColisionado)
    {
        //Nothing
    }

    public override ObjectType TipoObjeto()
    {
        return ObjectType.HEALTH_RECOVERY;
    }

    public override int TipoObjetoTienda()
    {
        return -1;
    }
}
