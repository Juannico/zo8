﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPick : PickableObject {

    public override void EfectoFinal(GameObject objetoColisionado)
    {
        throw new System.NotImplementedException();
    }

    public override void EfectoInstantaneo(GameObject objetoColisionado)
    {
        if (objetoColisionado.tag == "Player")
        {
            PlayerData.InstanciaData.monedas += 10;
            PlayerData.InstanciaData.Save();
            gameObject.SetActive(false);
        } 
    }

    public override void EfectoSobreTiempo(GameObject objetoColisionado)
    {
        throw new System.NotImplementedException();
    }

    public override ObjectType TipoObjeto()
    {
        return ObjectType.COIN;
    }

    public override int TipoObjetoTienda()
    {
        return -1;
    }
}
