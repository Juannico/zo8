﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoGrenade : PickableObject {
    public override void EfectoFinal(GameObject objetoColisionado)
    {
        throw new NotImplementedException();
    }

    public override void EfectoInstantaneo(GameObject objetoColisionado)
    {
        if (objetoColisionado.CompareTag("Player"))
        {
            objetoColisionado.GetComponent<CharacController>().balasGrenades += 1;
            gameObject.SetActive(false);
        }
    }

    public override void EfectoSobreTiempo(GameObject objetoColisionado)
    {
        throw new NotImplementedException();
    }

    public override ObjectType TipoObjeto()
    {
        return ObjectType.AMMO_GREN;
    }

    public override int TipoObjetoTienda()
    {
        return AMMUNITION;
    }
}
