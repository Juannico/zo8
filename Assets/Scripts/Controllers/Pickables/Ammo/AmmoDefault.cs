﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoDefault : PickableObject
{
    public override void EfectoFinal(GameObject objetoColisionado)
    {
        gameObject.SetActive(false);
    }

    public override void EfectoInstantaneo(GameObject objetoColisionado)
    {
        if (objetoColisionado.CompareTag("Player"))
        {
            objetoColisionado.GetComponent<CharacController>().balasNormalDisp += 15;
            gameObject.SetActive(false);
        }
    }

    public override void EfectoSobreTiempo(GameObject objetoColisionado)
    {
        //Nothing
    }

    public override ObjectType TipoObjeto()
    {
        return ObjectType.AMMO_N;
    }

    public override int TipoObjetoTienda()
    {
        return AMMUNITION;
    }
}
