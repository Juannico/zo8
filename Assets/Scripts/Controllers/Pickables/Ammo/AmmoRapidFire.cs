﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoRapidFire : PickableObject {

    public override void EfectoFinal(GameObject objetoColisionado)
    {
        throw new System.NotImplementedException();
    }

    public override void EfectoInstantaneo(GameObject objetoColisionado)
    {
        if (objetoColisionado.CompareTag("Player"))
        {
            objetoColisionado.GetComponent<CharacController>().balasRapidFire += 30;
            gameObject.SetActive(false);
        }
    }

    public override void EfectoSobreTiempo(GameObject objetoColisionado)
    {
        throw new System.NotImplementedException();
    }

    public override ObjectType TipoObjeto()
    {
        return ObjectType.AMMO_RF;
    }

    public override int TipoObjetoTienda()
    {
        return AMMUNITION;
    }

}
