﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenZombieSpawner : Spawner {
    
    public override void CondicionDeReaparicion()
    {
        if(Time.time - lastUpdate >= tiempoEntreSpawns && cantidadAcumulada > 0)
        {
            GameObject temp = getPooledObject();
            if (temp != null)
            {
                temp.transform.position = transform.position;
                temp.transform.rotation = transform.rotation;
                temp.SetActive(true);
                lastUpdate = Time.time;
            }

        }
    }

    public override void CondicionDeReaparicion(Transform trans)
    {
        throw new NotImplementedException();
    }
}
