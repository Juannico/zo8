﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableSpawner : Spawner
{
    public override void CondicionDeReaparicion(Transform trans, int quant)
    {
        base.CondicionDeReaparicion(trans, quant);
        for (int i = quant; i > 0; i--)
        {
            GameObject temp = getPooledObject();
            if (temp != null)
            {
                temp.transform.position = trans.position;
                temp.transform.rotation = transform.rotation;
                temp.SetActive(true);
                lastUpdate = Time.time;
            }
        }
        
    }

    public override void CondicionDeReaparicion(Transform spawnPosition)
    {
            GameObject temp = getPooledObject();
            if (temp != null)
            {
                temp.transform.position = spawnPosition.position;
                temp.transform.rotation = transform.rotation;
                temp.SetActive(true);
                lastUpdate = Time.time;
            }
    }

    public override void CondicionDeReaparicion()
    {
        //nothing
    }

    public override void Desaparicion()
    {
        base.Desaparicion();
    }
}
