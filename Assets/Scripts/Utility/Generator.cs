using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spanw : MonoBehaviour
{


    [SerializeField]
    private GameObject cube;
    [SerializeField]
     private GameObject sphere;

     private int limit1 = 6;
     private int limit2 = 6;
   

    void Start()
    {   
        ObjPooling.Preload(cube, limit1);
        ObjPooling.Preload(sphere, limit2);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Vector3 vector = SpawnPosition();

            GameObject c = ObjPooling.GetObject(cube);
            c.transform.position = vector;
            StartCoroutine(DeSpawn(cube, c, 3f));
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            Vector3 vector = SpawnPosition();

            GameObject s = ObjPooling.GetObject(sphere);
            s.transform.position = vector;
            StartCoroutine(DeSpawn(sphere, s, 3f));
        }
    }

    Vector3 SpawnPosition()
    {
        float x = Random.Range(-15f, 15f);
        float y = 3f;
        float z = Random.Range(-15f, 15f);

        Vector3 vector = new Vector3(x, y, z);

        return vector;
    }

    IEnumerator DeSpawn(GameObject primitive, GameObject go, float time)
    {
        yield return new WaitForSeconds(time);
        ObjPooling.RecicleObject(primitive, go);
    }
}
