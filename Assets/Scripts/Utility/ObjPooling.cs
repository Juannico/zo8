using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ObjPooling : MonoBehaviour
{
    private static ObjPooling instance;

    static Dictionary<int, Queue<GameObject>> pool = new Dictionary<int, Queue<GameObject>>();
    static Dictionary<int, GameObject> parents = new Dictionary<int, GameObject>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
        }
    }

    public static void Preload(GameObject objecToPool, int amount)
    {
        int id = objecToPool.GetInstanceID();

         GameObject parent = new GameObject();
        parent.name = objecToPool.name + "Pool";
        parents.Add(id, parent);

        pool.Add(id, new Queue<GameObject>());


        for (int i = 0; i < amount; i++)
        {
            CreateObject(objecToPool);
        }  
    }

    static void CreateObject(GameObject objecToPool)
    {
        int id = objecToPool.GetInstanceID();

        GameObject go = Instantiate(objecToPool) as GameObject;
        go.transform.SetParent(GetParent(id).transform);
        go.SetActive(false);

        pool[id].Enqueue(go);
    }

    static GameObject GetParent(int ParentId)
    {
        GameObject parent;
        parents.TryGetValue(ParentId, out parent);

        return parent;
    }

    public static GameObject GetObject(GameObject objectToPool) 
    { 
        int id = objectToPool.GetInstanceID();

        if (pool[id].Count == 0)
        {
            CreateObject(objectToPool);
        }

        GameObject go = pool[id].Dequeue();
        go.SetActive(true);

        return go;  
    }
    public static void RecicleObject(GameObject objectToPool, GameObject objectToRecicle)
    {
        int id = objectToPool.GetInstanceID();

        pool[id].Enqueue(objectToRecicle);
        objectToRecicle.SetActive(false);
    }
}
