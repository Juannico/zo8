﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using UnityEngine.Advertisements;


public class ShopManager : MonoBehaviour {

    public CharacterDatabase personajes;

    public CharacterList listaPersonajes;
    public ColorList listaColores;
    public PerkList listaPerks;
    public AmmoList listaMunicion;

    [Header("UI")]
    public Text monedasCounter;
    public Button playGame;
    public Button charButton;
    public Button ammoButton;
    public Button perkButton;

    protected ShopList openList;
    private MenuState menu;

    protected const int monedasPorADS = 100;


    // Use this for initialization
    void Start () {
        menu = GameObject.FindGameObjectWithTag("MenuState").GetComponent<MenuState>();
        PlayerData.Create();

        personajes.Cargar();

        openList = listaPersonajes;
        listaColores.Open();
        openList.Open();
        charButton.image.sprite = charButton.spriteState.pressedSprite;
        ammoButton.image.sprite = ammoButton.spriteState.disabledSprite;
        perkButton.image.sprite = perkButton.spriteState.disabledSprite;
    }

    public void AbrirListaPersonajesColores()
    {
        openList.Close();
        listaPersonajes.Open();
        listaColores.Open();
        openList = listaPersonajes;
        charButton.image.sprite = charButton.spriteState.pressedSprite;
        ammoButton.image.sprite = ammoButton.spriteState.disabledSprite;
        perkButton.image.sprite = perkButton.spriteState.disabledSprite;
    }

    public void AbrirListaAmmo()
    {
        if (openList == listaPersonajes)
        {
            listaColores.Close();
        }
        openList.Close();
        openList = listaMunicion;
        openList.Open();
        
        charButton.image.sprite = charButton.spriteState.disabledSprite;
        ammoButton.image.sprite = ammoButton.spriteState.pressedSprite;
        perkButton.image.sprite = perkButton.spriteState.disabledSprite;
    }

    public void AbrirListaPerks()
    {
        if (openList == listaPersonajes)
        {
            listaColores.Close();
        }
        openList.Close();
        openList = listaPerks;
        openList.Open();
        
        charButton.image.sprite = charButton.spriteState.disabledSprite;
        ammoButton.image.sprite = ammoButton.spriteState.disabledSprite;
        perkButton.image.sprite = perkButton.spriteState.pressedSprite;
    }

    public void ChangeCharacter(int dir)
    {
        menu.ChangeCharacter(dir);
        if (openList == listaPersonajes)
        {
            listaColores.Open();
        }
    }

    // Update is called once per frame
    void Update ()
    {
        monedasCounter.text = PlayerData.InstanciaData.monedas.ToString();
	}

    public void CloseScene(int state)
    {
        menu.CambioEstadoInterfaz(state);
        SceneManager.UnloadSceneAsync(2); 
    }

    public void ChangeColor(int co, string nomCol)
    {
        menu.CambioColor(co);
        if (PlayerData.InstanciaData.skins.Contains(nomCol))
        {
            playGame.interactable = true;
        }
        else playGame.interactable = false;
    }

    public void ShowRewardedAd()
    {
        //if (Advertisement.IsReady("rewardedVideo"))
        //{
        //    var options = new ShowOptions { resultCallback = HandleShowResult };
        //    Advertisement.Show("rewardedVideo", options);
        //}
    }

    //public void HandleShowResult(ShowResult result)
    //{
    //    if (result == ShowResult.Finished)
    //    {
    //        PlayerData.InstanciaData.monedas += monedasPorADS;
    //        PlayerData.InstanciaData.Save();
    //        openList.Populate();
    //    }
    //    else if (result == ShowResult.Failed)
    //    {

    //    }
    //}
}
