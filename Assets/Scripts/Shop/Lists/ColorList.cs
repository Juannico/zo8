﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorList : ShopList
{
    public Sprite disabledSprite;
    public Sprite normalSprite;

    public override void Populate()
    {
        r_RefreshCallBack = null;
        foreach (Transform item in listRoot)
        {
            Destroy(item.gameObject);
        }
        Character c = CharacterDatabase.ObtenerPersonaje(PlayerData.InstanciaData.personajes[PlayerData.InstanciaData.personajeActual]);
        if (c != null && c.colores != null && c.colores.Length > 0)
        {
            for (int i = 0; i < c.colores.Length; i++)
            {
                Colores actual = c.colores[i];
                GameObject nuevoItem = Instantiate(prefabItem);
                nuevoItem.transform.SetParent(listRoot, false);

                ShopItemListItem itm = nuevoItem.GetComponent<ShopItemListItem>();

                itm.icon.sprite = actual.icon;
                itm.buyButton.onClick.AddListener(delegate
                {
                    Equip(actual.nomCompuesto, actual.numeroColor, actual, c);
                });
            }
            s_manager.ChangeColor(c.colorActual, c.colores[c.colorActual].nomCompuesto);
            RefreshButton(c.colores[c.colorActual]);
        }
    }

    protected void RefreshButton(Colores col)
    {
        if (PlayerData.InstanciaData.skins.Contains(col.nomCompuesto))
        {
            externalButton.interactable = false;
            externalButton.transform.GetChild(0).GetComponent<Image>().sprite = disabledSprite;
            externalButton.transform.GetChild(1).GetComponent<Text>().text = "Owned";
            externalButton.onClick.RemoveAllListeners();
        }
        else
        {
            externalButton.transform.GetChild(1).GetComponent<Text>().text = col.precio.ToString();
            if (col.precio > PlayerData.InstanciaData.monedas)
            {
                externalButton.interactable = false;
                externalButton.transform.GetChild(0).GetComponent<Image>().sprite = disabledSprite;
                externalButton.transform.GetChild(1).GetComponent<Text>().color = Color.red;
                externalButton.onClick.RemoveAllListeners();
            }
            else
            {
                externalButton.transform.GetChild(0).GetComponent<Image>().sprite = normalSprite;
                externalButton.transform.GetChild(1).GetComponent<Text>().color = Color.black;
                externalButton.interactable = true;
                externalButton.onClick.RemoveAllListeners();
                externalButton.onClick.AddListener(delegate
                {
                    Buy(col);

                });
            }
        }

        


    }

    public void Buy(Colores col)
    {
        PlayerData.InstanciaData.monedas -= col.precio;
        PlayerData.InstanciaData.AddColor(col.nomCompuesto);
        PlayerData.InstanciaData.Save();
        Populate();
    }

    public void Equip(string colNom, int colNum, Colores col, Character c)
    {
        c.colorActual = colNum;
        s_manager.ChangeColor(colNum, colNom);
        RefreshButton(col);
    }
}
