﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoList : ShopList {

    static public PickableObject.ObjectType[] s_ObjectTypes = System.Enum.GetValues(typeof(PickableObject.ObjectType)) as PickableObject.ObjectType[];

    public override void Populate()
    {
        r_RefreshCallBack = null;
        foreach (Transform t in listRoot)
        {
            Destroy(t.gameObject);
        }

        for (int i = 0; i < s_ObjectTypes.Length; ++i)
        {
            PickableObject c = PickableDatabase.ObtenerAmmos(s_ObjectTypes[i]);
            if (c != null)
            {
                GameObject newEntry = Instantiate(prefabItem);
                newEntry.transform.SetParent(listRoot, false);

                ShopItemListItem itm = newEntry.GetComponent<ShopItemListItem>();

                itm.buyButton.image.sprite = itm.buyButtonSprite;

                itm.nameText.text = "x" + c.cantidad;

                itm.priceText.text = c.precio.ToString();

                itm.icon.sprite = c.icon;

                itm.countText.gameObject.SetActive(true);

                itm.buyButton.onClick.AddListener(delegate () { Buy(c,c.cantidad); });
                r_RefreshCallBack += delegate () { RefreshButton(itm, c); };
                RefreshButton(itm, c);
            }
        }
    }

    protected void RefreshButton(ShopItemListItem itemList, PickableObject c)
    {
        int count = 0;
        PlayerData.InstanciaData.objetosComprados.TryGetValue(c.TipoObjeto(), out count);
        itemList.countText.text = count.ToString();

        if (c.precio > PlayerData.InstanciaData.monedas)
        {
            itemList.buyButton.interactable = false;
            itemList.priceText.color = Color.red;
        }
        else
        {
            itemList.priceText.color = Color.black;
        }
    }

    public void Buy(PickableObject c,int canti)
    {
        PlayerData.InstanciaData.monedas -= c.precio;
        PlayerData.InstanciaData.AddObjeto(c.TipoObjeto(),canti);
        PlayerData.InstanciaData.Save();
        Refresh();
    }
}
