﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ShopList : MonoBehaviour {

    public GameObject prefabItem;
    public RectTransform listRoot;
    public Button externalButton;
    public ShopManager s_manager;

    public delegate void RefreshCallBack();

    protected RefreshCallBack r_RefreshCallBack;

    public void Open()
    {
        Populate();
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
        r_RefreshCallBack = null;
    }

    public void Refresh()
    {
        r_RefreshCallBack();
    }

    public abstract void Populate();
}
