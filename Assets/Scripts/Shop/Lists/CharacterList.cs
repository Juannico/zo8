﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterList : ShopList {

    public override void Populate()
    {
        r_RefreshCallBack = null;
        foreach (Transform item in listRoot)
        {
            Destroy(item.gameObject);
        }


        foreach (KeyValuePair<string, Character> pair in CharacterDatabase.diccionario)
        {
            Character c = pair.Value;
            if (c != null)
            {
                GameObject nuevoItem = Instantiate(prefabItem);
                nuevoItem.transform.SetParent(listRoot, false);

                ShopItemListItem itm = nuevoItem.GetComponent<ShopItemListItem>();

                itm.icon.sprite = c.icon;
                itm.priceText.text = c.costo.ToString();

                itm.buyButton.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().sprite = itm.buyButtonSprite;

                itm.buyButton.onClick.AddListener(delegate
                {
                    Buy(c);
                });

                r_RefreshCallBack += delegate
                {
                    RefreshButton(itm,c);
                };
                RefreshButton(itm, c);
            }

        }
    }

    protected void RefreshButton(ShopItemListItem item, Character chara)
    {
        if (chara.costo > PlayerData.InstanciaData.monedas)
        {
            item.buyButton.interactable = false;
            item.priceText.color = Color.red;
        }
        else item.priceText.color = Color.black;

        if (PlayerData.InstanciaData.personajes.Contains(chara.aliasPersonaje))
        {
            item.buyButton.interactable = false;
            item.buyButton.transform.GetChild(0).GetComponent<UnityEngine.UI.Image>().sprite = item.disabledButtonSprite;
            item.priceText.GetComponent<UnityEngine.UI.Text>().text = "Owned";
        }
    }

    public void Buy(Character c)
    {
        PlayerData.InstanciaData.monedas -= c.costo;
        PlayerData.InstanciaData.AddPersonaje(c.aliasPersonaje);
        PlayerData.InstanciaData.AddColor(c.colores[0].nomCompuesto);
        PlayerData.InstanciaData.Save();
        Populate();
    }
}
