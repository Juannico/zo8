﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class UIButtonHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public int actionCode;
    public Image cargarCambioBala;
    private GameState game;

    bool isPressed;
    public void OnPointerDown(PointerEventData eventData)
    {
        isPressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPressed = false;
        cargarCambioBala.fillAmount = 0;
        game.StopCharShooting();
    }

    // Use this for initialization
    void Start () {
        game = GameObject.FindGameObjectWithTag("GameState").GetComponent<GameState>();
	}
	
	// Update is called once per frame
	void Update () {
        if (isPressed)
        {
            if (actionCode < 100)
            {
                if (cargarCambioBala.fillAmount < 1)
                {
                    cargarCambioBala.fillAmount += .9f;
                }
                else
                {
                    game.CambioEstadoUI(actionCode);
                    isPressed = false;
                    cargarCambioBala.fillAmount = 0;
                }
            }
            else if (actionCode == 101)
            {
                game.MakeCharShoot();
            }   
        }
    }
}
